-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for quiz_application
DROP DATABASE IF EXISTS `quiz_application`;
CREATE DATABASE IF NOT EXISTS `quiz_application` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `quiz_application`;

-- Dumping structure for table quiz_application.answers
DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(800) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT 0,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_answer_question1_idx` (`question_id`),
  CONSTRAINT `fk_answer_question1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.answers: ~82 rows (approximately)
DELETE FROM `answers`;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` (`id`, `answer`, `is_correct`, `question_id`) VALUES
	(1, 'Ar1 q1', 1, 1),
	(2, 'Ar2 q1', 0, 1),
	(3, 'Ar3 q1', 0, 1),
	(4, 'Ar4 q1', 0, 1),
	(5, 'Ar1 q2', 1, 2),
	(6, 'Ar2 q2', 1, 2),
	(7, 'Ar3 q2', 0, 2),
	(8, 'Ar4 q2', 0, 2),
	(9, 'Ar5 q2', 0, 2),
	(10, 'Ar1 q3', 0, 3),
	(11, 'Ar2 q3', 1, 3),
	(12, 'Ar3 q3', 0, 3),
	(13, 'Ar4 q3', 0, 3),
	(14, 'Ar1 q4', 0, 4),
	(15, 'Ar2 q4', 0, 4),
	(16, 'Ar3 q4', 1, 4),
	(17, 'Ar4 q4', 0, 4),
	(18, 'Ar1 q5', 0, 5),
	(19, 'Ar2 q5', 0, 5),
	(20, 'Ar3 q5', 0, 5),
	(21, 'Ar4 q5', 1, 5),
	(22, 'Ar1 q6', 0, 6),
	(23, 'Ar2 q6', 1, 6),
	(24, 'Ar3 q6', 0, 6),
	(25, 'Ar4 q6', 0, 6),
	(26, 'Ar1 q7', 1, 7),
	(27, 'Ar2 q7', 1, 7),
	(28, 'Ar3 q7', 1, 7),
	(29, 'Ar4 q7', 1, 7),
	(30, 'Ar1 q8', 1, 8),
	(31, 'Ar2 q8', 0, 8),
	(32, 'Ar3 q8', 0, 8),
	(33, 'Ar4 q8', 0, 8),
	(34, 'Ar1 q9', 1, 9),
	(35, 'Ar2 q9', 0, 9),
	(36, 'Ar3 q9', 1, 9),
	(37, 'Ar4 q9', 0, 9),
	(38, 'Ar5 q9', 1, 9),
	(39, 'Ar1 q10', 0, 10),
	(40, 'Ar2 q10', 1, 10),
	(41, 'Ar3 q10', 0, 10),
	(42, 'Ar4 q10', 0, 10),
	(43, 'Ar1 q11', 0, 11),
	(44, 'Ar2 q11', 0, 11),
	(45, 'Ar3 q11', 1, 11),
	(46, 'Ar4 q11', 0, 11),
	(47, 'Ar1 q12', 0, 12),
	(48, 'Ar2 q12', 0, 12),
	(49, 'Ar3 q12', 1, 12),
	(50, 'Ar1 q13', 0, 13),
	(51, 'Ar2 q13', 1, 13),
	(52, 'Ar3 q13', 0, 13),
	(53, 'Ar4 q13', 0, 13),
	(54, 'Ar1 q14', 1, 14),
	(55, 'Ar2 q14', 1, 14),
	(56, 'Ar3 q14', 1, 14),
	(57, 'Ar4 q14', 1, 14),
	(58, 'Ar5 q14', 1, 14),
	(59, 'Ar1 q15', 1, 15),
	(60, 'Ar2 q15', 1, 15),
	(61, 'Ar3 q15', 1, 15),
	(62, 'Ar4 q15', 0, 15),
	(63, 'Ar1 q16', 0, 16),
	(64, 'Ar2 q16', 1, 16),
	(65, 'Ar3 q16', 0, 16),
	(66, 'Ar4 q16', 0, 16),
	(67, 'Ar1 q17', 1, 17),
	(68, 'Ar2 q17', 0, 17),
	(69, 'Ar3 q17', 0, 17),
	(70, 'Ar4 q17', 1, 17),
	(71, 'Ar1 q18', 0, 18),
	(72, 'Ar2 q18', 1, 18),
	(73, 'Ar3 q18', 0, 18),
	(74, 'Ar4 q18', 0, 18),
	(75, 'Ar1 q19', 1, 19),
	(76, 'Ar2 q19', 0, 19),
	(77, 'Ar3 q19', 0, 19),
	(78, 'Ar4 q19', 0, 19),
	(79, 'Ar1 q20', 1, 20),
	(80, 'Ar2 q20', 1, 20),
	(81, 'Ar3 q20', 1, 20),
	(82, 'Ar4 q20', 0, 20);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;

-- Dumping structure for table quiz_application.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(200) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_UNIQUE` (`category`),
  KEY `fk_category_user1_idx` (`author_id`),
  CONSTRAINT `fk_category_user1` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.categories: ~12 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `category`, `author_id`) VALUES
	(1, 'SQL', 1),
	(2, 'JavaScript', 1),
	(3, 'Java', 1),
	(4, 'C++', 1),
	(5, 'React', 1),
	(10, 'Pyton', 1),
	(13, 'Ada', 1),
	(14, 'C Sharp', 1),
	(17, 'Angular', 3),
	(18, 'PL SQL', 3),
	(19, 'PHP', 26),
	(20, 'MySQL', 26);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table quiz_application.questions
DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(1000) NOT NULL,
  `points` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_question_type1_idx` (`type_id`),
  KEY `fk_question_user1_idx` (`author_id`),
  KEY `fk_question_category1_idx` (`category_id`),
  CONSTRAINT `fk_question_category1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_type1` FOREIGN KEY (`type_id`) REFERENCES `question_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_user1` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.questions: ~20 rows (approximately)
DELETE FROM `questions`;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` (`id`, `question`, `points`, `type_id`, `author_id`, `category_id`) VALUES
	(1, 'Qn1 Ada', 2, 1, 1, 13),
	(2, 'Qn2 Ada', 6, 2, 1, 13),
	(3, 'Qn3 Ada', 4, 1, 1, 13),
	(4, 'Qn4 Ada', 3, 1, 1, 13),
	(5, 'Qn5 Ada', 3, 1, 1, 13),
	(6, 'Qn1 MySQL', 3, 1, 1, 20),
	(7, 'Qn2 MySQL', 5, 2, 1, 20),
	(8, 'Qn3 MySQL', 5, 1, 1, 20),
	(9, 'Qn4 MySQL', 6, 2, 1, 20),
	(10, 'Qn5 MySQL', 2, 1, 1, 20),
	(11, 'Qn6 MySQL', 3, 1, 1, 20),
	(12, 'Qn1 JavaScript', 1, 1, 1, 2),
	(13, 'Qn2 JavaScript', 2, 1, 1, 2),
	(14, 'Qn3 JavaScript', 3, 2, 1, 2),
	(15, 'Qn4 JavaScript', 4, 2, 1, 2),
	(16, 'Qn5 JavaScript', 5, 1, 1, 2),
	(17, 'Qn6 JavaScript', 6, 2, 1, 2),
	(18, 'Qn7 JavaScript', 1, 1, 1, 2),
	(19, 'Qn8 JavaScript', 2, 1, 1, 2),
	(20, 'Qn9 JavaScript', 5, 2, 1, 2);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;

-- Dumping structure for table quiz_application.question_types
DROP TABLE IF EXISTS `question_types`;
CREATE TABLE IF NOT EXISTS `question_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_UNIQUE` (`question_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.question_types: ~2 rows (approximately)
DELETE FROM `question_types`;
/*!40000 ALTER TABLE `question_types` DISABLE KEYS */;
INSERT INTO `question_types` (`id`, `question_type`) VALUES
	(2, 'multiple choice'),
	(1, 'single choice');
/*!40000 ALTER TABLE `question_types` ENABLE KEYS */;

-- Dumping structure for table quiz_application.quizzes
DROP TABLE IF EXISTS `quizzes`;
CREATE TABLE IF NOT EXISTS `quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_title` varchar(1000) NOT NULL,
  `time_limit_minutes` int(11) NOT NULL,
  `full_points` int(11) NOT NULL DEFAULT 0,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `is_visible` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `quiz_UNIQUE` (`quiz_title`),
  KEY `fk_quiz_user1_idx` (`author_id`),
  KEY `fk_quiz_category1_idx` (`category_id`),
  CONSTRAINT `fk_quiz_category1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_quiz_user1` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.quizzes: ~28 rows (approximately)
DELETE FROM `quizzes`;
/*!40000 ALTER TABLE `quizzes` DISABLE KEYS */;
INSERT INTO `quizzes` (`id`, `quiz_title`, `time_limit_minutes`, `full_points`, `author_id`, `category_id`, `is_visible`) VALUES
	(1, 'JS Fundamentals', 20, 25, 1, 2, 0),
	(2, 'SQL Basic', 25, 38, 1, 1, 0),
	(3, 'C# Fundamentals', 25, 66, 3, 14, 0),
	(4, 'SQL Qz1', 30, 52, 26, 1, 1),
	(5, 'SQL Qz2', 38, 92, 26, 1, 1),
	(6, 'SQL Qz3', 25, 54, 26, 1, 1),
	(7, 'JavaScript Qz1', 25, 57, 1, 2, 1),
	(8, 'JavaScript Qz2', 20, 80, 48, 2, 1),
	(9, 'JavaScript Qz3', 25, 84, 48, 2, 1),
	(10, 'Java QZ1', 20, 58, 1, 3, 1),
	(11, 'Java QZ2', 20, 60, 1, 3, 1),
	(12, 'Java QZ3', 20, 62, 1, 3, 1),
	(13, 'Java QZ4', 25, 68, 1, 3, 0),
	(14, 'C++ Qz1', 25, 59, 3, 4, 1),
	(15, 'C++ Qz2', 20, 57, 3, 4, 1),
	(16, 'C++ Qz3', 25, 49, 3, 4, 1),
	(17, 'React Qz1', 30, 82, 26, 5, 1),
	(18, 'React Qz2', 25, 59, 26, 5, 1),
	(19, 'React Qz3', 20, 37, 26, 5, 1),
	(20, 'Pyton Qz1', 25, 58, 1, 10, 1),
	(21, 'Pyton Qz3', 25, 90, 1, 10, 1),
	(22, 'Pyton Qz2', 25, 47, 1, 10, 1),
	(23, 'Ada Qz1', 30, 47, 3, 13, 1),
	(24, 'Ada Qz2', 30, 58, 3, 13, 1),
	(25, 'Ada Qz3', 20, 56, 1, 13, 0),
	(28, 'C# Qz1', 25, 69, 1, 14, 1),
	(29, 'C# Qz2', 29, 69, 1, 14, 1),
	(30, 'C# Qz3', 40, 98, 1, 14, 1),
	(31, 'React Qz4', 25, 74, 26, 5, 1);
/*!40000 ALTER TABLE `quizzes` ENABLE KEYS */;

-- Dumping structure for table quiz_application.quiz_has_questions
DROP TABLE IF EXISTS `quiz_has_questions`;
CREATE TABLE IF NOT EXISTS `quiz_has_questions` (
  `quiz_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`quiz_id`,`question_id`),
  KEY `fk_quiz_has_question_question1_idx` (`question_id`),
  KEY `fk_quiz_has_question_quiz1_idx` (`quiz_id`),
  CONSTRAINT `fk_quiz_has_question_question1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_quiz_has_question_quiz1` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.quiz_has_questions: ~11 rows (approximately)
DELETE FROM `quiz_has_questions`;
/*!40000 ALTER TABLE `quiz_has_questions` DISABLE KEYS */;
INSERT INTO `quiz_has_questions` (`quiz_id`, `question_id`) VALUES
	(1, 12),
	(1, 13),
	(1, 14),
	(1, 15),
	(1, 16),
	(7, 12),
	(7, 14),
	(7, 16),
	(7, 17),
	(7, 18),
	(7, 19);
/*!40000 ALTER TABLE `quiz_has_questions` ENABLE KEYS */;

-- Dumping structure for table quiz_application.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_UNIQUE` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.roles: ~2 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role`) VALUES
	(1, 'student'),
	(2, 'teacher');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table quiz_application.solved_quiz_answers
DROP TABLE IF EXISTS `solved_quiz_answers`;
CREATE TABLE IF NOT EXISTS `solved_quiz_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_solved_quiz_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_solved_quiz_answers_user_solved_quiz1_idx` (`user_solved_quiz_id`),
  KEY `fk_solved_quiz_answers_answer1_idx` (`answer_id`),
  CONSTRAINT `fk_solved_quiz_answers_answer1` FOREIGN KEY (`answer_id`) REFERENCES `answers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_solved_quiz_answers_user_solved_quiz1` FOREIGN KEY (`user_solved_quiz_id`) REFERENCES `user_solved_quizzes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.solved_quiz_answers: ~0 rows (approximately)
DELETE FROM `solved_quiz_answers`;
/*!40000 ALTER TABLE `solved_quiz_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `solved_quiz_answers` ENABLE KEYS */;

-- Dumping structure for table quiz_application.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(500) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_user_role_idx` (`role_id`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.users: ~17 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `role_id`) VALUES
	(1, 'marina', '$2b$10$oNU1mA/DyqifweY6pP9Q.OuYx2NkKjxbPtOVIKzSyT0TdhpOFgBDO', 'Marina', 'Gateva', 2),
	(2, 'gero', '$2b$10$u4ajV/ctuoI3/3dhtVVlf.irSz0G8MYL8gT3zzT6OvFJ3eZcaimgS', 'Gerasim', 'Mateev', 1),
	(3, 'racho', '$2b$10$xIEJx3mjSymPVgdzJUk6LOCZxjt8hLwAjB/09azKJHFz0nH917iFu', 'Rangel', 'Stoev', 2),
	(21, 'filip', '$2b$10$u5AzOtIZp.xr0FAWQvsMKu.124tkw2cmMUW0hL3NWfjz/R8LFF1ye', 'Filip', 'Manev', 1),
	(26, 'milko', '$2b$10$boYZC6uDZYhSi8vl5.ZZ/.b/AwW6Rb83wrE4Ako.j5pfqzLQr.U6G', 'Milko', 'Stoev', 2),
	(27, 'bobo', '$2b$10$Us0id1DhploVY6/J9dfGEewH7BQorTzIjVQNnW5zYyRGcn6zuGywe', 'Bobo', 'Bobev', 1),
	(29, 'bobi', '$2b$10$MrqVSfOYPYMyqdEWqaZqMu42MeE3WwhgJLNbJJulTV1HhCmtZcEmq', 'Boris', 'Krachunov', 1),
	(46, 'mitko', '$2b$10$jsCFl8slFykEd64V.i/5B.hubvKPeoQDlPfWgb87ZT9i7VslSI8xq', 'Dimitar', 'Lalov', 1),
	(47, 'filyo', '$2b$10$fNwMrZkNsqydRBvqNHCg5.jgvd1NDbmC8aoBALUyK/544NvI.qLP2', 'Filip', 'Arnaudov', 1),
	(48, 'neli', '$2b$10$Y1EMJpH3axGhQX6V3Kj0oukNbx5u6Nmoxx/Z5Y.LU3eyvEjz5emzq', 'Neli', 'NIkolova', 2),
	(50, 'nelka', '$2b$10$HKx.T/JTzhfAdAzenItDAecyJ0IrL1LnsemaM0eF75uWc0fZb.ES.', 'Nelina', 'Mitova', 1),
	(51, 'chichi', '$2b$10$2vSRrjIXHQ0oblFB4VicXea/wqEIllnO0bXvlezxIlxy7rHYw1/Ze', 'Metody', 'Totev', 1),
	(52, 'stef', '$2b$10$.WT6oXr1ARlWkqtOTGGpcuOznAzgNK1b7GmPOpzUpQG.onGLbCXbS', 'Stefan', 'Tconev', 1),
	(53, 'stamat', '$2b$10$kNbjFESVKxVj5nGcOfx/ueDxdk9LANFDKhho75kZ3AqrNQ3mecaz6', 'Stamat', 'Gruev', 1),
	(58, 'stiks', '$2b$10$3wzHOvmIwtMM3c1G8E1.B.u1ChSu6hgjtQMkwQ1z/hJpfRxoATTIW', 'Stilian', 'Krastev', 1),
	(63, 'misho', '$2b$10$3Xu8.elgCtYfqxc6e7iSCOwjmbQNQbp6BpTOI6ZFiqTdv7kTMb9f.', 'Mihail', 'Kostov', 1),
	(64, 'dinko', '$2b$10$bLMVeFildZ5dulcHh15Eye87FHiFBkm9qzvCg70a5fbpO/NjwzUX6', 'Dinko', 'Lambov', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table quiz_application.user_solved_quizzes
DROP TABLE IF EXISTS `user_solved_quizzes`;
CREATE TABLE IF NOT EXISTS `user_solved_quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_score` int(11) NOT NULL DEFAULT 0,
  `start_time` datetime NOT NULL DEFAULT current_timestamp(),
  `end_time` datetime NOT NULL DEFAULT current_timestamp(),
  `quiz_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_solved_quiz_quiz1_idx` (`quiz_id`),
  KEY `fk_user_solved_quiz_user1_idx` (`user_id`),
  CONSTRAINT `fk_user_solved_quiz_quiz1` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_solved_quiz_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table quiz_application.user_solved_quizzes: ~20 rows (approximately)
DELETE FROM `user_solved_quizzes`;
/*!40000 ALTER TABLE `user_solved_quizzes` DISABLE KEYS */;
INSERT INTO `user_solved_quizzes` (`id`, `total_score`, `start_time`, `end_time`, `quiz_id`, `user_id`) VALUES
	(1, 49, '2021-07-20 17:34:48', '2021-07-20 17:55:48', 16, 2),
	(2, 55, '2021-07-21 16:24:48', '2021-07-21 16:24:48', 18, 2),
	(3, 37, '2021-07-23 13:10:38', '2021-07-23 13:29:38', 19, 2),
	(4, 57, '2021-08-21 13:10:38', '2021-08-21 13:29:38', 15, 29),
	(5, 49, '2021-08-23 13:00:38', '2021-08-23 13:19:38', 16, 29),
	(6, 58, '2021-08-23 13:00:38', '2021-08-23 13:20:38', 20, 21),
	(7, 45, '2021-08-23 13:00:38', '2021-08-23 13:25:38', 28, 51),
	(8, 57, '2021-08-23 13:00:38', '2021-08-23 13:30:38', 24, 58),
	(9, 90, '2021-08-23 13:00:38', '2021-08-23 13:40:38', 30, 52),
	(10, 59, '2021-08-23 14:00:38', '2021-08-23 14:22:38', 14, 2),
	(11, 56, '2021-08-24 13:00:38', '2021-08-24 13:20:38', 15, 2),
	(12, 82, '2021-07-22 13:00:38', '2021-07-22 13:26:38', 17, 2),
	(13, 60, '2021-08-24 14:20:38', '2021-08-24 14:36:38', 11, 2),
	(14, 60, '2021-08-24 14:20:38', '2021-08-24 14:40:38', 11, 27),
	(15, 60, '2021-08-24 14:20:38', '2021-08-24 14:40:38', 11, 46),
	(16, 58, '2021-08-24 14:20:38', '2021-08-24 14:40:38', 11, 47),
	(17, 55, '2021-08-24 14:20:38', '2021-08-24 14:40:38', 11, 50),
	(18, 60, '2021-08-24 14:20:38', '2021-08-24 14:40:38', 11, 53),
	(19, 52, '2021-08-24 14:20:38', '2021-08-24 14:40:38', 11, 63),
	(20, 50, '2021-09-24 14:20:38', '2021-09-24 14:40:38', 28, 27);
/*!40000 ALTER TABLE `user_solved_quizzes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
