[![Quiz Logo](stuff/images/quizLogo15.png)](http://localhost:3000/)
# Quiz Application

## 1. Description

This is a web application that can be used to effectively create and carry out online quizzes.

The application has two main parts:
- Teacher’s dashboard – here, teachers can create categories, quizzes, and check students' performance
- Student's dashboard – students solve quizzes and view leaderboard.

## 2. Project information
  - Language and version: JavaScript ES2020
  - Platform and version: Node 14.0+
  - Core Packages: Express.js, React.js

## 3. Project architecture
  - database part  - MariaDB server
  - server part - build on Express.js package
  - client part - build on React.js package

## 4. Setup
###    4.1 Database
1. Create the database in a database tool, such as MySQL Workbench, from a file
```txt
    quiz-application/stuff/DB/quiz_application_database.sql
```

<!-- 2. Import the data into the database from a file
```txt
    quiz-application/stuff/DB/quiz_application_database_data.sql
``` -->

3. You can see the database chart in a file
   
```txt
	quiz-application/stuff/DB/quiz_application.png
```

###    4.2 Back-End
1. Open with VSCode folder `quiz-application/api`
2. Create `.env` file with the following content:
```js
    PORT=5500

    DB_HOST=localhost
    DB_USER=root
    DB_PORT=3306
    DB_PASSWORD=
    DATABASE=quiz_application

    SECRET_KEY=kjlksvdg353256ghzvolldnh55247
    TOKEN_LIFETIME = 2678400
```

1. Add your database password in `DB_PASSWORD`
2. Run `npm install` to restore all dependencies
3. Run `npm start` to start the server
4. Can find Postman Collection in 
   
```txt
    quiz-application/stuff/QuizApplication.postman_collection.json
```

###    4.3 Front-End
1. Open with VSCode folder `quiz-application/client`
2. Run `npm install` to restore all dependencies
3. Run `npm start` to start the application


## 5. Users
  - Registration form create only `students`.
  - For explore the full functionality can use:

```txt
  Teacher: 
    Username: marina
    Password: 123456
```  
```txt
  Student:  
    Username: gero
    Password: 123456
```

<br>
<br>

# Be Happy :)