import pool from './pool.js';

const getAllQuizzes = async () => {
  const sql = `
    SELECT q.id, q.quiz_title, q.time_limit_minutes, q.full_points, 
          q.author_id, concat_ws(' ', u.first_name, u.last_name) AS author_name, u.username,
          q.category_id, c.category, is_visible
      FROM quizzes AS q
      JOIN categories AS c
      JOIN users AS u
      WHERE q.category_id = c.id
      AND q.author_id = u.id
    `;

  const result = await pool.query(sql);

  return result;
};

const getAllVisibleQuizzes = async () => {
  const sql = `
    SELECT q.id, q.quiz_title, q.time_limit_minutes, q.full_points, 
          q.author_id, concat_ws(' ', u.first_name, u.last_name) AS author_name, u.username,
          q.category_id, c.category, is_visible
      FROM quizzes AS q
      JOIN categories AS c
      JOIN users AS u
      WHERE q.category_id = c.id
      AND q.author_id = u.id
      AND is_visible = 1
    `;

  const result = await pool.query(sql);

  return result;
};

const getQuizzesByCategory = async (category_id) => {
  const sql = `
    SELECT q.id, q.quiz_title, q.time_limit_minutes, q.full_points, 
          q.author_id, concat_ws(' ', u.first_name, u.last_name) AS author,
          q.category_id, c.category, is_visible
      FROM quizzes AS q
      JOIN categories AS c
      JOIN users AS u
      WHERE q.category_id = c.id
      AND q.author_id = u.id
      AND category_id = ?
      AND is_visible = 1
    `;

  const result = await pool.query(sql, [category_id]);

  return result;
};

const getQuizById = async (id) => {
  const sql = `
    SELECT q.id, q.quiz_title, q.time_limit_minutes, q.full_points, 
          q.author_id, concat_ws(' ', u.first_name, u.last_name) AS author,
          q.category_id, c.category, is_visible
      FROM quizzes AS q
      JOIN categories AS c
      JOIN users AS u
      WHERE q.category_id = c.id
      AND q.author_id = u.id
      AND q.id = ?
      AND is_visible = 1
    `;

  const result = await pool.query(sql, [id]);

  return result[0];
};

const createQuiz = async (data) => {
  const { quiz_title, time_limit_minutes, author_id, category_id } = data;

  const sql = `
    INSERT INTO quizzes(quiz_title, time_limit_minutes, author_id, category_id)
      VALUES (?, ?, ?, ?)
    `;

  const result = await pool.query(sql, [quiz_title, time_limit_minutes, author_id, category_id]);
  const newQuiz = await getQuizById(result.insertId);

  return newQuiz;
};

const checkForDuplicateQuiz = async (quiz_title) => {
  const sql = `
    SELECT quiz_title
        FROM quizzes
        WHERE quiz_title = ?
    `;

  const result = await pool.query(sql, [quiz_title]);

  return result[0];
};

const updateQuiz = async (data) => {
  const { id, quiz_title, time_limit_minutes, full_points, author_id, category_id, is_visible } = data;

  const sql = `
    UPDATE quizzes SET 
        quiz_title = ?,
        time_limit_minutes = ?,
        full_points = ?,
        author_id = ?,
        category_id = ?,
        is_visible = ?
      WHERE id = ?
    `;

  await pool.query(sql, [quiz_title, time_limit_minutes, full_points, author_id, category_id, is_visible, id]);
  const newQuiz = await getQuizById(id);

  return newQuiz;
};

const checkForDuplicateQuizExclusiveId = async (quiz_title, id) => {
  const sql = `
    SELECT quiz_title
        FROM quizzes
        WHERE quiz_title = ?
        AND id != ?
    `;

  const result = await pool.query(sql, [quiz_title, id]);

  return result[0];
};


// from table 'user_solved_quizzes'

const getAllSolvedQuizzes = async () => {
  const sql = `
    SELECT usq.id, usq.total_score, usq.start_time, usq.end_time, 
          usq.quiz_id, q.quiz_title, q.full_points, q.category_id, c.category,
          usq.user_id, concat_ws(' ', u.first_name, u.last_name ) AS student, u.username  
      FROM user_solved_quizzes AS usq
      JOIN quizzes AS q
      JOIN categories AS c
      JOIN users AS u
      WHERE usq.quiz_id = q.id
      AND q.category_id = c.id
      AND usq.user_id = u.id
      AND q.is_visible = 1
  `;

  const result = await pool.query(sql);

  return result;
};

const getQuizzesSolvedByUser = async (user_id) => {
  const sql = `
    SELECT usq.id, usq.total_score, usq.start_time, usq.end_time, 
          usq.quiz_id, q.quiz_title, q.full_points, q.category_id, c.category,
          usq.user_id, concat_ws(' ', u.first_name, u.last_name ) AS student, u.username  
      FROM user_solved_quizzes AS usq
      JOIN quizzes AS q
      JOIN categories AS c
      JOIN users AS u
      WHERE usq.quiz_id = q.id
      AND q.category_id = c.id
      AND usq.user_id = u.id
      AND usq.user_id = ?
      AND q.is_visible = 1
      ORDER BY usq.end_time DESC
  `;

  const result = await pool.query(sql, [user_id]);

  return result;
};

const getQuizzesSolvedByUserByCategory = async (user_id, category_id) => {
  const sql = `
    SELECT usq.id, usq.total_score, usq.start_time, usq.end_time, 
          usq.quiz_id, q.quiz_title, q.full_points, q.category_id, c.category,
          usq.user_id, concat_ws(' ', u.first_name, u.last_name ) AS student, u.username  
      FROM user_solved_quizzes AS usq
      JOIN quizzes AS q
      JOIN categories AS c
      JOIN users AS u
      WHERE usq.quiz_id = q.id
      AND q.category_id = c.id
      AND usq.user_id = u.id
      AND usq.user_id = ?
      AND category_id = ?
      AND q.is_visible = 1
  `;

  const result = await pool.query(sql, [user_id, category_id]);

  return result;
};


export default {
  getAllQuizzes,
  getAllVisibleQuizzes,
  getQuizzesByCategory,
  getQuizById,
  createQuiz,
  checkForDuplicateQuiz,
  updateQuiz,
  checkForDuplicateQuizExclusiveId,

  getAllSolvedQuizzes,
  getQuizzesSolvedByUser,
  getQuizzesSolvedByUserByCategory,
};