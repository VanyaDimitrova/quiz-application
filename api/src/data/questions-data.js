import pool from './pool.js';

const getAllQuestions = async () => {
  const sql = `
    SELECT q.id, q.question, q.points, q.type_id, t.question_type,
      q.author_id, concat_ws(' ', u.first_name, u.last_name) AS author,
      q.category_id, c.category
      FROM questions AS q
      JOIN categories AS c
      JOIN users AS u
      JOIN question_types AS t
      WHERE q.category_id = c.id
      AND q.author_id = u.id
      AND q.type_id = t.id
    `;

  const result = await pool.query(sql);

  return result;
};

const getQuestionsByCategory = async (category_id) => {
  const sql = `
    SELECT q.id, q.question, q.points, q.type_id, t.question_type,
      q.author_id, concat_ws(' ', u.first_name, u.last_name) AS author,
      q.category_id, c.category
      FROM questions AS q
      JOIN categories AS c
      JOIN users AS u
      JOIN question_types AS t
      WHERE q.category_id = c.id
      AND q.author_id = u.id
      AND q.type_id = t.id
      AND q.category_id = ?
    `;

  const result = await pool.query(sql, [category_id]);

  return result;
};

const getQuestionById = async (id) => {
  const sql = `
    SELECT q.id, q.question, q.points, q.type_id, t.question_type,
      q.author_id, concat_ws(' ', u.first_name, u.last_name) AS author,
      q.category_id, c.category
      FROM questions AS q
      JOIN categories AS c
      JOIN users AS u
      JOIN question_types AS t
      WHERE q.category_id = c.id
      AND q.author_id = u.id
      AND q.type_id = t.id
      AND q.id = ?
    `;

  const result = await pool.query(sql, [id]);

  return result[0];
};

const createQuestion = async (data) => {
  const { question, points, type_id, author_id, category_id } = data;

  const sql = `
    INSERT INTO questions(question, points, type_id, author_id, category_id)
      VALUES (?, ?, ?, ?, ?)
    `;

  const result = await pool.query(sql, [question, points, type_id, author_id, category_id]);
  const newQuestion = await getQuestionById(result.insertId);

  return newQuestion;
};

const updateQuestion = async (data) => {
  const { id, question, points, type_id, author_id, category_id } = data;

  const sql = `
    UPDATE questions SET 
        question = ?,
        points = ?,
        type_id = ?,
        author_id = ?,
        category_id = ?
      WHERE id = ?
    `;

  await pool.query(sql, [question, points, type_id, author_id, category_id, id]);
  const newQuestion = await getQuestionById(id);

  return newQuestion;
};

const checkForDuplicateQuestion = async (question) => {
  const sql = `
    SELECT question
        FROM questions
        WHERE question = ?
    `;

  const result = await pool.query(sql, [question]);

  return result[0];
};

export default {
  getAllQuestions,
  getQuestionsByCategory,
  getQuestionById,
  createQuestion,
  updateQuestion,
  checkForDuplicateQuestion,
};