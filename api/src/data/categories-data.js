import pool from './pool.js';

const getAllCategories = async () => {
  const sql = `
    SELECT c.id, c.category, c.author_id, concat_ws(' ', u.first_name, u.last_name) AS author
      FROM categories AS c
      JOIN users AS u
      WHERE c.author_id = u.id
      ORDER BY c.id DESC
    `;

  const result = await pool.query(sql);

  return result;
};

const getCategoryById = async (id) => {
  const sql = `
    SELECT c.id, c.category, c.author_id, concat_ws(' ', u.first_name, u.last_name) AS author
      FROM categories AS c
      JOIN users AS u
      WHERE c.author_id = u.id
      AND c.id = ?
    `;

  const result = await pool.query(sql, [id]);

  return result[0];
};

const createCategory = async (data) => {
  const { category, author_id } = data;

  const sql = `
    INSERT INTO categories(category, author_id)
      VALUES (?, ?)
    `;

  const result = await pool.query(sql, [category, author_id]);
  const newCategory = await getCategoryById(result.insertId);

  return newCategory;
};

const updateCategory = async (data) => {
  const { id, category, author_id } = data;

  const sql = `
    UPDATE categories SET 
        category = ?,
        author_id = ?
      WHERE id = ?
    `;

  await pool.query(sql, [category, author_id, id]);
  const newCategory = await getCategoryById(id);

  return newCategory;
};

const checkForDuplicateCategory = async (value) => {
  const sql = `
    SELECT category
        FROM categories
        WHERE category = ?
    `;

  const result = await pool.query(sql, [value]);

  return result[0];
};

const checkForDuplicateCategoryExclusiveId = async (category, id) => {
  const sql = `
    SELECT category
        FROM categories
        WHERE category = ?
        AND id != ?
    `;

  const result = await pool.query(sql, [category, id]);

  return result[0];
};

const checkForExistsCategory = async (value) => {
  const sql = `
    SELECT category
        FROM categories
        WHERE id = ?
    `;

  const result = await pool.query(sql, [value]);

  return result[0];
};

export default {
  getAllCategories,
  getCategoryById,
  createCategory,
  updateCategory,
  checkForDuplicateCategory,
  checkForDuplicateCategoryExclusiveId,
  checkForExistsCategory,
};