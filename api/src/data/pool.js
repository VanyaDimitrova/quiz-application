import mariadb from 'mariadb';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;

const pool = mariadb.createPool({

  port: +config.DB_PORT,
  database: config.DATABASE,
  user: config.DB_USER,
  password: config.DB_PASSWORD,
  host: config.DB_HOST,
  connectionLimit: 2,

});

export default pool;
