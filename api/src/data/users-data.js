import pool from './pool.js';

const getUserWithRole = async (username) => {
  const sql = `
    SELECT u.id, u.username, u.password, u.role_id, r.role, 
          u.first_name, u.last_name, concat_ws(' ', u.first_name, u.last_name) AS full_name
        FROM users AS u
        JOIN roles AS r 
        WHERE u.role_id = r.id
        AND u.username = ? 
    `;

  const result = await pool.query(sql, [username]);

  return result[0];
};

const getUserById = async (id) => {
  const sql = `
  SELECT u.id, u.username, u.password, u.role_id, r.role, 
        u.first_name, u.last_name, concat_ws(' ', u.first_name, u.last_name) AS author
      FROM users AS u
      JOIN roles AS r 
      WHERE u.role_id = r.id
      AND u.id = ? 
  `;

  const result = await pool.query(sql, [id]);

  return result[0];
};

const checkForDuplicateUsername = async (column, value) => {
  const sql = `
    SELECT username
        FROM users
        WHERE ${column}=?
    `;

  const result = await pool.query(sql, [value]);

  return result[0];
};

const createUser = async (data) => {
  const { username, passwordHash, first_name, last_name, DEFAULT_USER_ROLE } = data;

  const sql = `
    INSERT INTO users(username, password, first_name, last_name, role_id)
        VALUES (?, ?, ?, ?, (SELECT id FROM roles WHERE role = ?))
    `;

  const result = await pool.query(sql, [username, passwordHash, first_name, last_name, DEFAULT_USER_ROLE]);

  return {
    id: result.insertId,
    username: username,
    first_name: first_name,
    last_name: last_name,
    role: DEFAULT_USER_ROLE
  };
};

export default {
  getUserWithRole,
  getUserById,
  checkForDuplicateUsername,
  createUser,
};
