export default {
  category: value => {
    if (!value) {
      return 'Category is required';
    }    
    if (typeof value !== 'string' || value.length < 2 || value.length > 200) {
      return 'Category should be a string in range [2..200]';
    }
    
    return null;
  },

  author_id: value => {
    if (!value || typeof value !== 'number') {
      return 'author_id with type number is required';
    }

    return null;
  },
};
