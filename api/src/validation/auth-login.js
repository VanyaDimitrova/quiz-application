export default {   
  username: value => { 
    if (!value) {
      return 'Username is required';
    }
        
    if (typeof value !== 'string' || value.length < 3 || value.length > 45) {
      return 'Username should be a string in range [3..45]';
    }

    return null;
  },
  password: value => {
    if (!value) {
      return 'Password is required';
    }
        
    if (typeof value !== 'string' || value.length < 3 || value.length > 45) {
      return 'Password should be a string in range [3..45]';
    }

    return null;
  },
};
