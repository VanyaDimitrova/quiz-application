import bcrypt from 'bcrypt';
import serviceErrors from './service-errors.js';
import { DEFAULT_USER_ROLE } from '../common/constants.js';

const signInUser = (usersData) => {
  return async (username, password) => {
    const user = await usersData.getUserWithRole(username);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.INVALID_SIGHIN,
        user: null
      };
    }

    return {
      error: null,
      user: user
    };
  };
};

const createUser = (usersData) => {
  return async (userCreate) => {
    const { username, password, first_name, last_name } = userCreate;

    const existingUser = await usersData.checkForDuplicateUsername('username', username);

    if (existingUser) {
      return {
        error: {
          error: serviceErrors.DUPLICATE_RECORD,
          data: userCreate
        },
        user: null
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.createUser({ username, passwordHash, first_name, last_name, DEFAULT_USER_ROLE });

    return { 
      error: null, 
      user: user 
    };
  };
};

export default {
  signInUser,
  createUser,
};
