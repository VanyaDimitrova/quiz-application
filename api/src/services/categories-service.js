import serviceErrors from './service-errors.js';


const getAllCategories = (categoriesData) => {
  return async () => {
    const categories = await categoriesData.getAllCategories();

    if (!categories[0]) {
      return {
        error: serviceErrors.NO_CONTENT,
        categories: null
      };
    }

    return {
      error: null,
      categories: categories
    };
  };
};

const createCategory = (categoriesData) => {
  return async (data) => {
    // const { category, author_id } = data;
    const existingCategory = await categoriesData.checkForDuplicateCategory(data.category);
    if (existingCategory) {
      return {
        error: {
          error: serviceErrors.DUPLICATE_RECORD,
          data: data.category
        },
        newCategory: null
      };
    }

    const newCategory = await categoriesData.createCategory(data);

    return {
      error: null,
      newCategory: newCategory
    };
  };
};

const updateCategory = (categoriesData) => {
  return async (data) => {
    const existingCategory = await categoriesData.checkForDuplicateCategoryExclusiveId(data.category, data.id);
    if (existingCategory) {
      return {
        error: {
          error: serviceErrors.DUPLICATE_RECORD,
          data: data.category
        },
        updatedCategory: null
      };
    }

    const updatedCategory = await categoriesData.updateCategory(data);

    return {
      error: null,
      updatedCategory: updatedCategory
    };
  };
};

export default {
  getAllCategories,
  createCategory,
  updateCategory,
};
