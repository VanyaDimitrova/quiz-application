import serviceErrors from './service-errors.js';


const getAllQuestions = (questionsData) => {
  return async () => {
    const questions = await questionsData.getAllQuestions();

    if (!questions[0]) {
      return {
        error: serviceErrors.NO_CONTENT,
        questions: null
      };
    }

    return {
      error: null,
      questions: questions
    };
  };
};

const getQuestionsByCategory = (questionsData, categoriesData) => {
  return async (category_id) => {
    const existsCategory = await categoriesData.checkForExistsCategory(category_id);
    if (!existsCategory) {
      return {
        error: {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: category_id
        },
        questionsByCategory: null
      };
    }

    const questionsByCategory = await questionsData.getQuestionsByCategory(category_id);

    return {
      error: null,
      questionsByCategory: questionsByCategory
    };
  };
};

const createQuestion = (questionsData) => {
  return async (data) => {

    const newQuestion = await questionsData.createQuestion(data);

    return {
      error: null,
      newQuestion: newQuestion
    };
  };
};

const updateQuestion = (questionsData) => {
  return async (data) => {

    const updatedQuestion = await questionsData.updateQuestion(data);

    return {
      error: null,
      updatedQuestion: updatedQuestion
    };
  };
};

export default {
  getAllQuestions,
  getQuestionsByCategory,
  createQuestion,
  updateQuestion,
};

