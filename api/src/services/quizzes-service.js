import serviceErrors from './service-errors.js';


const getAllQuizzes = (quizzesData) => {
  return async () => {
    const quizzes = await quizzesData.getAllQuizzes();

    if (!quizzes[0]) {
      return {
        error: serviceErrors.NO_CONTENT,
        quizzes: null
      };
    }

    return {
      error: null,
      quizzes: quizzes
    };
  };
};

const getQuizzesByCategory = (quizzesData, categoriesData) => {
  return async (category_id) => {
    const existsCategory = await categoriesData.checkForExistsCategory(category_id);
    if (!existsCategory) {
      return {
        error: {
          error: serviceErrors.RECORD_NOT_FOUND,
          data: category_id
        },
        quizzesByCategory: null
      };
    }

    const quizzesByCategory = await quizzesData.getQuizzesByCategory(category_id);

    return {
      error: null,
      quizzesByCategory: quizzesByCategory
    };
  };
};

const createQuiz = (quizzesData) => {
  return async (data) => {

    const existingQuiz = await quizzesData.checkForDuplicateQuiz(data.quiz_title);
    if (existingQuiz) {
      return {
        error: {
          error: serviceErrors.DUPLICATE_RECORD,
          data: data.quiz_title
        },
        newQuiz: null
      };
    }

    const newQuiz = await quizzesData.createQuiz(data);

    return {
      error: null,
      newQuiz: newQuiz
    };

  };
};

const updateQuiz = (quizzesData) => {
  
  return async (data) => {
    const existingQuiz = await quizzesData.checkForDuplicateQuizExclusiveId(data.quiz_title, data.id);
    if (existingQuiz) {
      return {
        error: {
          error: serviceErrors.DUPLICATE_RECORD,
          data: data.quiz_title
        },
        newQuiz: null
      };
    }

    const updatedQuiz = await quizzesData.updateQuiz(data);

    return {
      error: null,
      updatedQuiz: updatedQuiz
    };
  };
};

const getAllSolvedQuizzes = (quizzesData) => {
  return async () => {
    const solvedQuizzes = await quizzesData.getAllSolvedQuizzes();

    if (!solvedQuizzes[0]) {
      return {
        error: serviceErrors.NO_CONTENT,
        solvedQuizzes: null
      };
    }

    return {
      error: null,
      solvedQuizzes: solvedQuizzes
    };
  };
};

const getQuizzesSolvedByUser = (quizzesData) => {
  return async (user_id) => {
    const solvedQuizzes = await quizzesData.getQuizzesSolvedByUser(user_id);

    if (!solvedQuizzes[0]) {
      return {
        error: serviceErrors.NO_CONTENT,
        solvedQuizzes: null
      };
    }

    return {
      error: null,
      solvedQuizzes: solvedQuizzes
    };
  };
};

const getQuizzesSolvedByUserByCategory = (quizzesData) => {
  return async (user_id, category_id) => {
    const solvedQuizzes = await quizzesData.getQuizzesSolvedByUserByCategory(user_id, category_id);

    if (!solvedQuizzes[0]) {
      return {
        error: serviceErrors.NO_CONTENT,
        solvedQuizzes: null
      };
    }

    return {
      error: null,
      solvedQuizzes: solvedQuizzes
    };
  };
};

export default {
  getAllQuizzes,
  getQuizzesByCategory,
  createQuiz,
  updateQuiz,
  getAllSolvedQuizzes,
  getQuizzesSolvedByUser,
  getQuizzesSolvedByUserByCategory,
};

