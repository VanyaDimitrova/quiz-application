import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;
const TOKEN_LIFETIME = dotenv.config().parsed.TOKEN_LIFETIME; // 60 * 60 * 24 * 31 = 2678400 seconds

const createToken = (payload) => {
  const token = jwt.sign(
    payload,
    SECRET_KEY,
    { expiresIn: TOKEN_LIFETIME }
  );

  return token;    
};

export default createToken;
