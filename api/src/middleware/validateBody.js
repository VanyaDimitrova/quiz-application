export default (schema) => {
  return (req, res, next) => {
    const body = req.body;
    const validations = Object.keys(schema);

    if (Object.keys(schema).length !== Object.keys(body).length) {
      res.status(400).send({ error: 'Invalid number of keys in body' });
      return;
    }

    const fails = validations
      .map(v => schema[v](body[v]))
      .filter(e => e !== null);

    if (fails.length > 0) {
      res.status(400).send({ message: fails.join(', ') });
    } else {
      next();
    }
  };
};
