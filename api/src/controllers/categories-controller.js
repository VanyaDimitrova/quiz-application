import express from 'express';
import { TEACHER_ROLE } from '../common/constants.js';
import categoriesData from '../data/categories-data.js';
import { roleMiddleware } from '../middleware/auth.middleware.js';
import validateBody from '../middleware/validateBody.js';
import categoriesService from '../services/categories-service.js';
import serviceErrors from '../services/service-errors.js';
import createCategoryValidator from '../validation/create-category.js';

const categoriesController = express.Router();

categoriesController
  .get('/', async (req, res) => {

    const { error, categories } = await categoriesService.getAllCategories(categoriesData)();

    res.status(200).send(categories);
  })
  .post('/', roleMiddleware(TEACHER_ROLE), validateBody(createCategoryValidator) , async (req, res) => {
    const data = req.body;

    const { error, newCategory } = await categoriesService.createCategory(categoriesData)(data); 

    if (error?.error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message: `Category ${error?.data} already exists.` });
    } else {
      res.status(201).send(newCategory);
    }
  })
  .put('/', roleMiddleware(TEACHER_ROLE), validateBody(createCategoryValidator), async (req, res) => {
    const data = req.body;

    const { error, updatedCategory } = await categoriesService.updateCategory(categoriesData)(data);

    if (error?.error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message: `Category ${error?.data} already exists.` });
    } else {
      res.status(201).send(updatedCategory);
    }
  });

export default categoriesController;
