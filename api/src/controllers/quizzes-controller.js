import express from 'express';
import categoriesData from '../data/categories-data.js';
import quizzesData from '../data/quizzes-data.js';
import quizzesService from '../services/quizzes-service.js';
import serviceErrors from '../services/service-errors.js';

const quizzesController = express.Router();

quizzesController
  .get('/', async (req, res) => {

    const { error, quizzes } = await quizzesService.getAllQuizzes(quizzesData)();

    if (error?.error === serviceErrors.NO_CONTENT) {
      res.status(204).send({ message: 'There is no quizzes.' });
    } else {
      res.status(200).send(quizzes);
    }
  })
  .get('/category/:id', async (req, res) => {
    const data = req.params.id;

    const { error, quizzesByCategory } = await quizzesService.getQuizzesByCategory(quizzesData, categoriesData)(data);

    if (error?.error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: `Category with id ${error.data} not found.` });
    } else {
      res.status(200).send(quizzesByCategory);
    }
  })
  .post('/', async (req, res) => {
    const data = req.body;

    const { error, newQuiz } = await quizzesService.createQuiz(quizzesData)(data); 

    if (error?.error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message: `Quiz ${error?.data} already exists.` });
    } else {
      res.status(201).send(newQuiz);
    }
  })
  .put('/', async (req, res) => {
    const data = req.body;

    const { error, updatedQuiz } = await quizzesService.updateQuiz(quizzesData)(data);

    if (error?.error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).send({ message: `Quiz ${error?.data} already exists.` });
    } else {
      res.status(201).send(updatedQuiz);
    } 
  })
  .get('/solved', async (rer, res) => {

    const { error, solvedQuizzes } = await quizzesService.getAllSolvedQuizzes(quizzesData)();

    if (error?.error === serviceErrors.NO_CONTENT) {
      res.status(204).send({ message: 'There is no solved quizzes.' });
    } else {
      res.status(200).send(solvedQuizzes);
    }
  })
  .get('/solved/:user_id', async (req, res) => {
    const data = req.params.user_id;

    const { error, solvedQuizzes } = await quizzesService.getQuizzesSolvedByUser(quizzesData)(data);

    if (error?.error === serviceErrors.NO_CONTENT) {
      res.status(204).send({ message: 'There is no solved quizzes.' });
    } else {
      res.status(200).send(solvedQuizzes);
    }
  })
  .get('/solved/:user_id/category/:category_id', async (req, res) => {
    const user_id = req.params.user_id;
    const category_id = req.params.category_id;

    const { error, solvedQuizzes } = await quizzesService.getQuizzesSolvedByUserByCategory(quizzesData)(user_id, category_id);

    if (error?.error === serviceErrors.NO_CONTENT) {
      res.status(204).send({ message: 'There is no solved quizzes.' });
    } else {
      res.status(200).send(solvedQuizzes);
    }
  });

export default quizzesController;