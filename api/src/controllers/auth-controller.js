import express from 'express';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/service-errors.js';
import usersService from '../services/users-service.js';
import createToken from './../auth/create-token.js';


const authController = express.Router();

authController 
  .post('/signin', async (req, res) => {
    const { username, password } = req.body;
    const { error, user } = await usersService.signInUser(usersData)(username, password);

    if (error === serviceErrors.INVALID_SIGHIN) {
      res.status(400).send({
        message: 'Invalid username or password.'
      });
    } else {
      const payload = {
        id: user.id,
        username: user.username,
        role: user.role,
        role_id: user.role_id,
        first_name: user.first_name,
        last_name: user.last_name,
        full_name: user.full_name,
      };

      const token = createToken(payload);

      res.status(200).send({
        token: token,
      });
    }
  });

export default authController;
