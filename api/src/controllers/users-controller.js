import express from 'express';
import usersData from '../data/users-data.js';
import usersService from '../services/users-service.js';
import errors from '../services/service-errors.js';
import validateBody from '../middleware/validateBody.js';
import createUserValidator from '../validation/create-user.js';

const usersController = express.Router();

usersController
  .post('/', validateBody(createUserValidator), 
    async (req, res,) => {
      const data = req.body;

      const { error, user } = await usersService.createUser(usersData)(data);

      if (error?.error === errors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Username \'' + `${error?.data.username}` + '\' already exists.' });
      } else {
        res.status(201).send(user);
      }
    });

export default usersController;
