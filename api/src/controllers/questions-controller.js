import express from 'express';
import categoriesData from '../data/categories-data.js';
import questionsData from '../data/questions-data.js';
import questionsService from '../services/questions-service.js';
import serviceErrors from '../services/service-errors.js';


const questionsController = express.Router();

questionsController
  .get('/', async (req, res) => {

    const { error, questions } = await questionsService.getAllQuestions(questionsData)();

    res.status(200).send(questions);
  })
  .get('/categories/:id', async (req, res) => {
    const data = req.params.id;

    const { error, questionsByCategory } = await questionsService.getQuestionsByCategory(questionsData, categoriesData)(data);

    if (error?.error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: `Category with id ${error.data} not found.` });
    } else {
      res.status(200).send(questionsByCategory);
    }
  })
  .post('/', async (req, res) => {
    const data = req.body;

    const { error, newQuestion } = await questionsService.createQuestion(questionsData)(data); 

    res.status(201).send(newQuestion);

  })
  .put('/', async (req, res) => {
    const data = req.body;

    const { error, updatedQuestion } = await questionsService.updateQuestion(questionsData)(data);

    res.status(201).send(updatedQuestion);

  });

export default questionsController;