
export const STUDENT_ROLE = 'student';
export const TEACHER_ROLE = 'teacher';
export const DEFAULT_USER_ROLE = STUDENT_ROLE;