import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';

import { authMiddleware } from './middleware/auth.middleware.js';

import authController from './controllers/auth-controller.js';
import usersController from './controllers/users-controller.js';
import categoriesController from './controllers/categories-controller.js';
import quizzesController from './controllers/quizzes-controller.js';
import questionsController from './controllers/questions-controller.js';


const app = express();

const config = dotenv.config().parsed;
const PORT = +config.PORT;

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use(express.json());
app.use(cors());
app.use(helmet());

app.use('/auth', authController);
app.use('/users', usersController);
app.use('/categories', authMiddleware, categoriesController);
app.use('/quizzes', authMiddleware, quizzesController);
app.use('/questions', authMiddleware, questionsController);

app.all('*', (req, res) => {
  res.status(400).json({
    message: 'Resource not found.'
  });
});

app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));
