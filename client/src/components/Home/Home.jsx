import './Home.css';
import React from 'react';
import { useContext } from 'react';
import AuthContext from './../../providers/AuthContext';
import { STUDENT_ROLE, TEACHER_ROLE } from './../../common/constants';
import StudentsDashboardPage from '../Students/Students dashboard page/StudentsDashboardPage';
import TeachersDashboardPage from '../Teachers/Teachers dashboard page/TeachersDashboardPage';

const Home = () => {
  const { user } = useContext(AuthContext);

  if (user.role === STUDENT_ROLE) {
    return (
      <StudentsDashboardPage />
    );
  }
  
  if (user.role === TEACHER_ROLE) {
    return (
      <TeachersDashboardPage />
    );
  }
};

export default Home;