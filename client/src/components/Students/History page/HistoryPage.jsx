import './History.css';
import React from 'react';
import AllQuizzesSolvedByStudent from './AllQuizzesSolvedByStudent';

const HistoryPage = () => {
  const end = 0;

  return (
    <div className="HistoryPage" >
      <h3>Quiz History</h3>
      <AllQuizzesSolvedByStudent end={end} />
    </div>
  );
};

export default HistoryPage;
