import './History.css';
import React from 'react';
import { useEffect, useState, useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';
import { getQuizzesSolvedByUser } from '../../../requests/QuizzesRequests';
import SingleSolvedQuiz from './SingleSolvedQuiz';


const AllQuizzesSolvedByStudent = ({ end }) => {
  const { user } = useContext(AuthContext);

  const [quizzesSolvedByUser, setQuizzesSolvedByUser] = useState([]);

  if (end === 0) {
    end = quizzesSolvedByUser.length;
  }

  useEffect(() => {
    getQuizzesSolvedByUser(user.id)
      .then((data) => setQuizzesSolvedByUser(data))
      .catch(e => console.log(e));;
  }, [user.id]);

  console.log('quizzesSolvedByUser', quizzesSolvedByUser);

  return (
    <div className="AllQuizzesSolvedByStudent">
      <div>
        {
          quizzesSolvedByUser.slice(0, end).map((quiz) => 
            <SingleSolvedQuiz key={quiz.id} quiz={quiz} />
          )
        }
      </div>
    </div>
  );
};

export default AllQuizzesSolvedByStudent;
