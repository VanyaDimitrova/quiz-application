import './SolveQuizPage.css';
import React from 'react';

const SolveQuizPage = ({ match }) => {
  const { id } = match.params;

  return (
    <div className="SolveQuizPage">
      <h3>Quiz id {id}</h3> 
    </div>
  );
};

export default SolveQuizPage;
