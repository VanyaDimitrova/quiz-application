import './Leaderboard.css';
import React from 'react';
import AllSolvedQuizzes from './AllSolvedQuizzes';

const AllStudentsLeaderboardPage = () => {
  const page = 'LeaderboardPage';

  return (
    <div className="LeaderboardPage">
      <h3>Leaderboard</h3>
      <AllSolvedQuizzes page={page} />
    </div>
  );
};

export default AllStudentsLeaderboardPage;
