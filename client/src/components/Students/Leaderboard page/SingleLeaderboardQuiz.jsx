import './Leaderboard.css';
import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const SingleLeaderboardQuiz = ({ leaderboard }) => {
  return (
    <Card border="secondary" style={{ marginBottom: '10px', marginTop: '15px'}}>
      <Card.Body style={{ padding: '0.3rem 0.8rem' }}> 
        <Card.Text>
          <Row>
            <Col style={{ color: 'var(--blue)', fontSize: '18px' }}>
              {leaderboard.username}
            </Col>
            <Col style={{ color: 'var(--blue)', fontSize: '16px' }}>
              {leaderboard.student}
            </Col>
            <Col style={{ color: 'var(--blue)', fontSize: '16px' }}>
              Score: {leaderboard.all_score} ( {leaderboard.all_possible_points} )
            </Col>
            <Col style={{ color: 'var(--blue)', fontSize: '16px' }}>
              Grade: {leaderboard.percentage}
            </Col>
            <Col style={{ color: 'var(--blue)', fontSize: '16px' }}>
              Quizzes: {leaderboard.count_quizzes}
            </Col>
          </Row>       
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default SingleLeaderboardQuiz;
