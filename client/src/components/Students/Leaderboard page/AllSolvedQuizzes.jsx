import './Leaderboard.css';
import React from 'react';
import { useEffect, useState } from 'react';
import { getAllSolvedQuizzes } from '../../../requests/QuizzesRequests';
import SingleLeaderboardQuiz from './SingleLeaderboardQuiz';


const AllSolvedQuizzes = ({ page }) => {
  const [sort, setSort] = useState('all_score');
  const [topNumber, setTopNumber] = useState(5);
  const [allSolvedQuizzes, setAllSolvedQuizzes] = useState([]);
  const [leaderboard, setLeaderboard] = useState([]);


  const fullPoint = async (all) => {
    const result = [];
    await all.forEach((el) => { 
      const resultIndex = result.findIndex((item) => item.user_id === el.user_id);
    
      if (resultIndex === -1) {
        result.push({
          user_id: el.user_id,
          username: el.username,
          student: el.student,
          count_quizzes: 1,
          all_score: Number(el.total_score),
          all_possible_points: Number(el.full_points),
          percentage: (Math.round(Number(el.total_score) * 100 / Number(el.full_points) * 100) / 100).toFixed(2),
        }); 
      } else {
        result[resultIndex].count_quizzes += 1;
        result[resultIndex].all_score += Number(el.total_score);
        result[resultIndex].all_possible_points += Number(el.full_points);
        result[resultIndex].percentage = (Math.round(Number(result[resultIndex].all_score) * 100 / Number(result[resultIndex].all_possible_points) * 100) / 100).toFixed(2);
      }
    });
  
    return result;
  };

  useEffect(() => {
    getAllSolvedQuizzes()
      .then((data) => setAllSolvedQuizzes(data));
  }, []);


  useEffect(() => {
    fullPoint(allSolvedQuizzes)
      .then((data) => {
        return data.sort((a, b) => b[sort] - a[sort]);
      })
      .then((data) => setLeaderboard(data))
      .catch(e => console.log(e));
  }, [allSolvedQuizzes, sort, leaderboard.length]);

  console.log('leaderboard', leaderboard);
 

  return (
    <div className="AllSolvedQuizzes">
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between', padding: '0 0.8rem 0 0.3rem' }} > 
          <form>
            <span>{'Sort by '}</span>
            <select
              onChange={(e) => setSort(e.target.value)}
            >
              <option value='all_score'>Score</option>
              <option value='percentage'>Grade</option>
            </select>
          </form>

          { page === 'Leaderboard'        
            ? <form>
              <span>{'Display top '}</span>
              <select
                onChange={(e) => setTopNumber(e.target.value)}
              >
                <option value='5'>5</option>
                <option value='10'>10</option>
                <option value='15'>15</option>
              </select>
              <span>{' students'}</span>
            </form>
            : null}
        </div>
        { page === 'Leaderboard' 
          ? leaderboard.slice(0, topNumber).map(student => 
            <SingleLeaderboardQuiz key={student.user_id} leaderboard={student} />
          )
          : leaderboard.map(student => 
            <SingleLeaderboardQuiz key={student.user_id} leaderboard={student} />
          )
        }
      </div>
    </div>
  );
};

export default AllSolvedQuizzes;
