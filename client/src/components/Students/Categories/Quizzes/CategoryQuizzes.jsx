import './Quizzes.css';
import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { getQuizzesByCategory, getQuizzesSolvedByUserByCategory } from '../../../../requests/QuizzesRequests';
import AuthContext from '../../../../providers/AuthContext';
import CategorySingleQuiz from './CategorySingleQuiz';
import CategorySingleSolvedQuiz from './CategorySingleSolvedQuiz';

const CategoryQuizzes = (props) => {
  
  const { id } = props.match.params;
  const queryParams = new URLSearchParams(props.location.search);
  const category =queryParams.get('category');
 
  console.log('props', category);

  const { user } = useContext(AuthContext);

  const [quizzes, setQuizzes] = useState([]);
  const [solvedQuizzes, setSolvedQuizzes] = useState([]);
  const [unsolvedQuizzes, setUnsolvedQuizzes] = useState([]);


  useEffect(() => {
    getQuizzesSolvedByUserByCategory(user.id, id)
      .then((data) => setSolvedQuizzes(data));

  }, [user.id, id]);

  console.log('solvedQuizzes', solvedQuizzes);

  useEffect(() => {
    getQuizzesByCategory(id)
      .then((data) => {
        setQuizzes(data);
        return data;
      })
      .then((data) => {
        const solvedQuizzesId = solvedQuizzes.map(q => q.quiz_id);
        const unsolved = data.filter(quiz => !solvedQuizzesId.includes(quiz.id));
        setUnsolvedQuizzes(unsolved);
      })
      .catch(er => console.log(er));

  }, [id, solvedQuizzes]);

  console.log('quizzes' ,quizzes);
  console.log('unsolvedQuizzes' ,unsolvedQuizzes);

  return (
    <div className="CategoryQuizzes">
      {quizzes.message
        ? <div  className="message">
          {quizzes.message}
        </div>
        : <div>
          <h3>Category {category}</h3>
          {quizzes.length === 0
            ? <div>
              <h2>There is no quizzes!</h2>
            </div>
            : <div className="QuizzesAll" >
              <div className="Quizzes" >
                <h4>Quizzes</h4>
                {unsolvedQuizzes.map(quiz => 
                  <CategorySingleQuiz key={quiz.id} quiz={quiz} />
                )}
              </div>
              <div className="QuizzesSolved" >
                <h4>Solved quizzes</h4>
                {solvedQuizzes.map(quiz => 
                  <CategorySingleSolvedQuiz key={quiz.id} quiz={quiz} />
                )}
              </div>
            </div>
          }
        </div>
      }
    </div>
  );
};

export default CategoryQuizzes;
