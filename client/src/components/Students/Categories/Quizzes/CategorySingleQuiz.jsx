import './Quizzes.css';
import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { Card, Row, Col, Modal, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const CategorySingleQuiz = ({ quiz }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Card border="primary" style={{ marginBottom: '10px', marginTop: '15px' }}>
        <Card.Body style={{ padding: '0.3rem 0.8rem' }}>
          <Card.Text>
            <Row>
              <Col xs={6} style={{ color: 'var(--blue)', fontSize: '19px' }}>
                {quiz.quiz_title}
              </Col>
              <Col style={{ color: 'var(--blue)', fontSize: '18px' }}>
              Points: {quiz.full_points}
              </Col>
              <Col style={{ color: 'var(--blue)', fontSize: '18px' }}>
              Time: {quiz.time_limit_minutes} min
              </Col>
              <Col xs={1}>
                <button className="btn-solve" onClick={handleShow}>
                Solve
                </button>
              </Col>
            </Row>
          </Card.Text>
        </Card.Body>
      </Card>
    
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>You has only one attempt</Modal.Title>
        </Modal.Header>
        <Modal.Body>Do you want to solve the quiz!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Link to={`/quizzes/solve/${quiz.id}`}>
            <Button variant="primary" onClick={handleClose}>
              Solve
            </Button>
          </Link>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default CategorySingleQuiz;
