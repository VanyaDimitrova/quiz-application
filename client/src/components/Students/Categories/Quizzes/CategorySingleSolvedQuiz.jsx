import './Quizzes.css';
import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const CategorySingleSolvedQuiz = ({ quiz }) => {
  return (
    <Card border="secondary" style={{ marginBottom: '10px', marginTop: '15px'}}>
      <Card.Body style={{ padding: '0.3rem 0.8rem' }}> 
        <Card.Text>
          <Row>
            <Col style={{ color: 'var(--blue)', fontSize: '19px' }}>
              {quiz.quiz_title}
            </Col>
            <Col style={{ color: 'var(--blue)', fontSize: '18px' }}>
              {new Date(quiz.end_time).toLocaleDateString('it-CA')}
            </Col>
            <Col style={{ color: 'var(--blue)', fontSize: '18px' }}>
              Score {quiz.total_score} ( {quiz.full_points} )
            </Col>
          </Row>       
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default CategorySingleSolvedQuiz;
