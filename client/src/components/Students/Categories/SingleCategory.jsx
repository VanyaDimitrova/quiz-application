import './Categories.css';
import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

const SingleCategory = ({ category }) => {
  return (
    <Card border="primary" style={{ marginBottom: '10px', marginTop: '15px'}}>
      <Card.Body style={{ padding: '0.3rem 0.8rem' }}> 
        <Card.Text>
          <Row>
            <Col>
              <div className="SingleCategory" >
                <Link to={`/categories/${category.id}?category=${category.category}`} >
                  {category.category}
                </Link>
              </div>
            </Col>
            <Col style={{ color: '#FFFFFF', fontSize: '20px' }}>A</Col>
          </Row>       
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default SingleCategory;
