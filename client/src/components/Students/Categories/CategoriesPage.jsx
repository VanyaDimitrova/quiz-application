import './Categories.css';
import React from 'react';
import AllCategories from './AllCategories';

const CategoriesPage = () => {
  return (
    <div className="CategoriesPage">
      <h3>Categories</h3>
      <AllCategories />
    </div>
  );
};

export default CategoriesPage;
