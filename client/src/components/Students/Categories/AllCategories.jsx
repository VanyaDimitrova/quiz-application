import './Categories.css';
import React from 'react';
import { useEffect, useState } from 'react';
import { getAllCategories } from '../../../requests/CategoriesRequests';
import SingleCategory from './SingleCategory';

const AllCategories = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getAllCategories()
      .then((data) => setCategories(data))
      .catch(e => console.log(e));
  }, []);

  console.log(categories);

  return (
    <div className="AllCategories">
      <div>
        {categories.map((category, i, arr) =>
          <SingleCategory key={category.id} category={category} categories={arr} />
        )}
      </div>
    </div>
  );
};

export default AllCategories;