import './StudentsDashboardPage.css';
import React from 'react';
import Leaderboard from './Leaderboard';
import QuizHistory from './QuizHistory';
import Categories from './Categories';

const StudentsDashboardPage = () => {

  return (
    <div className="App StudentsDashboard">
      <div className="CategoriesAll">
        <Categories />
      </div>
      <div className="LiederQuiz">
        <Leaderboard />
        <QuizHistory />
      </div>
    </div>
  );
};

export default StudentsDashboardPage;
