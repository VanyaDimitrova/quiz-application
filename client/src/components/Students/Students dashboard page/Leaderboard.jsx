import './StudentsDashboardPage.css';
import React from 'react';
import { Link } from 'react-router-dom';
import AllSolvedQuizzes from '../LeaderBoard page/AllSolvedQuizzes';


const Leaderboard = () => {
  const page = 'Leaderboard';

  return (
    <div className="Leaderboard">
      <Link to={'/leaderboard'}> 
        <h3>Leaderboard</h3>
      </Link>
      <AllSolvedQuizzes page={page} />
    </div>
  );
};

export default Leaderboard;