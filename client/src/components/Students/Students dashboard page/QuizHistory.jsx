import './StudentsDashboardPage.css';
import React from 'react';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import AllQuizzesSolvedByStudent from '../History page/AllQuizzesSolvedByStudent';
import AuthContext from './../../../providers/AuthContext';

const QuizHistory = () => {
  const { user } = useContext(AuthContext);

  return (
    <div className="QuizHistory">
      <Link to={'/history'}> 
        <h4>Quizzes solved by 
          <span style={{ color: 'var(--blue)', fontSize: '23px' }}>
            {' ' + user.username}
          </span>
        </h4>
      </Link>
      <AllQuizzesSolvedByStudent end={5} />
    </div>
  );
};

export default QuizHistory;