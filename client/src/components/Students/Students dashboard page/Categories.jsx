import './StudentsDashboardPage.css';
import React from 'react';
import { Link } from 'react-router-dom';
import AllCategories from '../Categories/AllCategories';


const Categories = () => {

  return (
    <div className="CategoriesBoard">
      <Link to={'/categories'}> 
        <h3>Categories</h3>
      </Link>
      <AllCategories />
    </div>
  );
};

export default Categories;