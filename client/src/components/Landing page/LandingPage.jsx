import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './LandingPage.css';
import LogIn from './LogIn';
import Register from './Register';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

const LandingPage = () => {
  return (
    <div className="App">
      <div className="LandingPage">
        <Tabs>
          <TabList>
            <Tab>LogIn</Tab>
            <Tab>Register</Tab>
          </TabList>

          <TabPanel style={{ width: '280px' }} >
            <h3>Login to your account</h3>
            <LogIn /> 
          </TabPanel>
          <TabPanel>
            <h3>Register new user</h3>
            <Register />
          </TabPanel>
        </Tabs>
      </div>
    </div>
  );
};

export default LandingPage;
