import React from 'react';
import { useContext, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import './LandingPage.css';
import { createUser } from '../../requests/UsersRequests';
import AuthContext from './../../providers/AuthContext';
import getUserFromToken from './../../services/auth/user-from-token';
import { loginUser } from './../../requests/UsersRequests';
import { useHistory } from 'react-router-dom';


const Register = () => {
  const { setUser } = useContext(AuthContext);
  const [registration, setRegistration] = useState({
    username: '',
    password: '',
    first_name: '',
    last_name: '',  
  });

  const create = (prop, value) => {
    registration[prop] = value;
    setRegistration({ ...registration });
  };

  const registerUser = (e, data) => {
    e.preventDefault();
    createUser(data)
      .then((response) => {
        console.log('row 30', response);
        if (response.status === 201) {
          setSuccessMessage(true);
        }
        if (response.status === 409) {
          setDuplicateMessage(true);
        }
        if (response.status === 400) {
          setErrorMessage(true);
        }
        return response.json();
      })
      .then((res) => {
        const message = res.message;
        const username = res.username;

        console.log('row 39 res', message, username);
        if (username) {
          setMessage(`User ${username} have registered successfully!`);
        }
        if (message) {
          setMessage(message);
        }
      })
      .catch(e => console.log(e.message));
  };

  const history = useHistory();

  const login = (e, data) => {
    e.preventDefault();
    loginUser(data)
      .then((res) => res.json())
      .then(response => {
        setUser(getUserFromToken(response.token));
        localStorage.setItem('token', response.token);
      })
      .then(() => history.push('/dashboard'));
  };


  const [duplicateMessage, setDuplicateMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [successMessage, setSuccessMessage] = useState(false);
  const [message, setMessage] = useState('');


  return (
    <div className="container-forms">
      <Form >
        <Form.Group as={Col} controlId="formBasicAddress">
          <div>
            <b>Account information</b>
          </div>
          <hr />
          <Row>
            <Form.Group as={Col} controlId="formBasicUsername">
              <Form.Control
                maxLength={15}
                onClick={() => {
                  setDuplicateMessage(false);
                  setErrorMessage(false);
                  setSuccessMessage(false);
                }}
                onChange={(e) => {
                  create('username', e.target.value);
                }}
                type="username"
                placeholder="Username"
              />
              <div style={{ fontSize: 13, marginTop: 5 }}>
                    Between 3 - 45 symbols
              </div>
            </Form.Group>
            {duplicateMessage ? (
              <div style={{ color: 'red' }}>{message}</div>
            ) : null}
            <Form.Group as={Col} controlId="formBasicPassword">
              <Form.Control
                maxLength={20}
                onClick={() => {
                  setDuplicateMessage(false);
                  setErrorMessage(false);
                  setSuccessMessage(false);
                }}
                onChange={(e) => {
                  create('password', e.target.value);
                }}
                type="password"
                placeholder="Password"
              />
              <div style={{ fontSize: 13, marginTop: 5 }}>
                    At least 3 symbols
              </div>
            </Form.Group>
          </Row>
        </Form.Group>
        <Form.Group as={Col} controlId="formBasicAddress">
          <div style={{ marginTop: '15px'}}>
            <b>Personal information</b>
          </div>
          <hr />
          <Row>
            <Form.Group as={Col} style={{ marginTop: '6px'}} controlId="formBasicFirstName">
              <Form.Control
                maxLength={20}
                onClick={() => {
                  setDuplicateMessage(false);
                  setErrorMessage(false);
                  setSuccessMessage(false);
                }}
                onChange={(e) => create('first_name', e.target.value)}
                type="name"
                placeholder="First Name"
              />
            </Form.Group>
            <Form.Group as={Col} style={{ marginTop: '6px'}} controlId="formBasicLastName">
              <Form.Control
                maxLength={20}
                onClick={() => {
                  setDuplicateMessage(false);
                  setErrorMessage(false);
                  setSuccessMessage(false);
                }}
                onChange={(e) => create('last_name', e.target.value)}
                type="name"
                placeholder="Last Name"
              />
            </Form.Group>
          </Row>
        </Form.Group>
        {errorMessage ? (
          <div style={{ color: 'red' }}>{message}</div>
        ) : null}
        {successMessage ? (
          <div style={{ textAlign: 'center' }}>
            <div style={{ color: 'green' }}>
              {message}
            </div>
            <Button
              variant="primary"
              onClick={(e) => {
                login(e, registration);
              }}
              type="submit"
            >
                  Login
            </Button>
          </div>
        ) : 
          (<div>
            {
              <Button
                className="btn-login"
                onClick={(e) => {
                  registerUser(e, registration);
                }}
              >
                    Register
              </Button>
            }
          </div>
          )}
      </Form>
    </div>
  );
};

export default Register;