import React from 'react';
import { useContext, useState } from 'react';
import { loginUser } from '../../requests/UsersRequests';
import AuthContext from '../../providers/AuthContext';
import { Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import getUserFromToken from '../../services/auth/user-from-token';
import './LandingPage.css';

const LogIn = () => {
  const { setUser } = useContext(AuthContext);
  const [errorMessage, setErrorMessage] = useState(false);
  const [loginInfo, setLoginInfo] = useState({
    username: '',
    password: '',
  });

  const collectLoginData = (prop, value) => {
    loginInfo[prop] = value;
    setLoginInfo({ ...loginInfo });
  };

  const history = useHistory();

  const login = (e, data) => {
    e.preventDefault();
    loginUser(data)
      .then((response) => {
        if (response.status === 400) {
          setErrorMessage(true);
          return null;
        }
        response.json()
          .then((res) => {
            console.log('res', res);
            const newUser = getUserFromToken(res.token);
            setUser(getUserFromToken(res.token));
            localStorage.setItem('token', res.token);
            return newUser;
          })
          .then(() => history.push('/dashboard'));
      }); 
  };

  return (
    <div className="container-forms"> 
      <Form>
        <Form.Group>
          <Form.Label>Username</Form.Label>
          <Form.Control
            maxLength={100}
            onClick={() => setErrorMessage(false)}
            type="username"
            placeholder="Enter username"
            onChange={(e) => {
              collectLoginData('username', e.target.value);
            }}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control
            maxLength={100}
            onClick={() => setErrorMessage(false)}
            onChange={(e) => {
              collectLoginData('password', e.target.value);
            }}
            type="password"
            placeholder="Password"
          />
        </Form.Group>
        {errorMessage ? (
          <div
            style={{
              color: 'black',
              marginBottom: '10px',
              fontSize: '20px',
            }}
          >
            Invalid username or password!
          </div>
        ) : null}
        <Button
          className="btn-login"
          onClick={(e) => {
            login(e, loginInfo);
          }}
          
          type="submit"
        >
          Login
        </Button>
      </Form>
    </div>
  );
};

export default LogIn;
