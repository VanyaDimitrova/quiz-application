import './CategoriesTeachers.css';
import React from 'react';
import AllCategoriesTeachers from './AllCategoriesTeachers';

const CategoriesPageTeachers = () => {
  return (
    <div className="CategoriesPageTeachers">
      <h3>Categories</h3>
      <AllCategoriesTeachers />
    </div>
  );
};

export default CategoriesPageTeachers;
