import './CategoriesTeachers.css';
import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { getQuizzesByCategory, getQuizzesSolvedByUserByCategory } from '../../../requests/QuizzesRequests';
import AuthContext from '../../../providers/AuthContext';
import { Button } from 'react-bootstrap';

const CategoryQuizzesTeacher = ({ match }) => {
  const { category, id } = match.params;

  const { user } = useContext(AuthContext);

  const [quizzes, setQuizzes] = useState([]);
  const [solvedQuizzes, setSolvedQuizzes] = useState([]);
  const [unsolvedQuizzes, setUnsolvedQuizzes] = useState([]);
  

  console.log('match', match);

  useEffect(() => {
    getQuizzesSolvedByUserByCategory(user.id, id)
      .then((data) => setSolvedQuizzes(data));

  }, [user.id, id]);

  console.log('solvedQuizzes', solvedQuizzes);

  useEffect(() => {
    getQuizzesByCategory(id)
      .then((data) => {
        setQuizzes(data);
        return data;
      })
      .then((data) => {
        const solvedQuizzesId = solvedQuizzes.map(q => q.quiz_id);
        const unsolved = data.filter(quiz => !solvedQuizzesId.includes(quiz.id));
        setUnsolvedQuizzes(unsolved);
      });

  }, [id, solvedQuizzes]);

  console.log('quizzes' ,quizzes);
  console.log('unsolvedQuizzes' ,unsolvedQuizzes);

  return (
    <div className="App">
      {quizzes.length === 0
        ? <div>
          <h3>There is no quizzes for category {category}</h3>
        </div>
        : <div>
          <h4>Quizzes from category {category}</h4>
          {unsolvedQuizzes.map(quiz => 
            <div key={quiz.id} >
              {quiz.quiz_title}
              <Button>
                Solve
              </Button>
              <Button>
                View
              </Button>
            </div>
          )}
        </div>
      }
    </div>
  );
};

export default CategoryQuizzesTeacher;
