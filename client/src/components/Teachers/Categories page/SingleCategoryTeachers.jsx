import './CategoriesTeachers.css';
import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


const SingleCategoryTeachers = ({ category }) => {
  return (
    <Card border="primary" style={{ marginBottom: '10px', marginTop: '15px'}}>
      <Card.Body style={{ padding: '0.3rem 0.8rem' }}> 
        <Card.Text>
          <Row>
            <Col>
              <div className="SingleCategoryTeachers" >
                <Link to={`/category/${category.category}/${category.id}`} >
                  {category.category}
                </Link>
              </div>
            </Col>
            <Col style={{ color: '#FFFFFF', fontSize: '20px' }}>{'_'}</Col>
          </Row>       
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default SingleCategoryTeachers;
