import './CategoriesTeachers.css';
import React, { useContext, useState } from 'react';
import { Card, Col } from 'react-bootstrap';
import AuthContext from './../../../providers/AuthContext';
import { createCategory } from '../../../requests/CategoriesRequests';
import 'bootstrap/dist/css/bootstrap.min.css';

const CreateCategory = ({ categories, setCategories }) => {
  const { user } = useContext(AuthContext);
  const [content, setContent] = useState('');
  const [newCategory, setNewCategory] = useState({
    category: '',
    author_id: user.id,
  });

  const create = (prop, value) => {
    newCategory[prop] = value;
    setNewCategory({ ...newCategory });
  };

  const createNewCategory = (e, data) => {
    e.preventDefault();
    createCategory(data)
      .then((res) => {
        if (res.message) {
          return alert(res.message);
        } else {
          setCategories([newCategory, ...categories]);
        }

        console.log('res123', res);
      })

      .then(() => {
        setContent('');
        setNewCategory({
          category: '',
          author_id: user.id,
        });
      })
      .catch(er => console.log('er.message', er.message));
  };

  return (
    <Card border="primary" style={{ marginBottom: '10px', marginTop: '15px', maxWidth: '40rem'}}>
      <Card.Body style={{ padding: '0.3rem 0.8rem' }}> 
        <Card.Text>
          <Col style={{ color: 'var(--blue)', fontSize: '20px' }}>
            Create new category
          </Col>
          <form onSubmit={(e) => createNewCategory(e, newCategory)} >
            <input 
              style={{ width: '70%', boxSizing: 'border-box' }}
              type="text" 
              placeholder="Category" 
              onChange={(e) => {
                setContent(e.target.value);
                create('category', e.target.value);
              }}
              value={content} 
            />
            <button type="submit" className="btn-create" >
                Create
            </button>
            <div style={{ fontSize: '10px', margin: '2px 8px', color: 'var(--blue)', 
              display: 'flex', justifyContent: 'start' }}>
                min 2 symbols
            </div>
          </form>
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default CreateCategory;