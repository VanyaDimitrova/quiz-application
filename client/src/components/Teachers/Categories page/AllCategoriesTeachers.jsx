import './CategoriesTeachers.css';
import React from 'react';
import { useEffect, useState } from 'react';
import { getAllCategories } from '../../../requests/CategoriesRequests';
import SingleCategoryTeachers from './SingleCategoryTeachers';
import CreateCategory from './CreateCategory';


const AllCategoriesTeachers = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getAllCategories()
      .then((data) => setCategories(data))
      .catch(e => console.log(e));
  }, []);

  console.log('categoriesTeacher', categories);

  return (
    <div className="AllCategoriesTeachers">
      <div>
        <CreateCategory categories={categories} setCategories={setCategories} />
      </div>
      <div>
        {categories.map(category =>
          <SingleCategoryTeachers key={category.id} category={category} />
        )}
      </div>
    </div>
  );
};

export default AllCategoriesTeachers;