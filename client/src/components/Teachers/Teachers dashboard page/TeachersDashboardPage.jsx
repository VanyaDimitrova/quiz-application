import './TeachersDashboardPage.css';
import React from 'react';
import CategoriesSection from './CategoriesSection';
import CreatedQuizzesSection from './CreatedQuizzesSection';

const TeachersDashboardPage = () => {

  return (
    <div className="App TeachersDashboard">
      <div className="CategoriesSection">
        <CategoriesSection />
      </div>
      <div className="CreatedQuizzesSection">
        <CreatedQuizzesSection />
      </div>
    </div>
  );
};

export default TeachersDashboardPage;
