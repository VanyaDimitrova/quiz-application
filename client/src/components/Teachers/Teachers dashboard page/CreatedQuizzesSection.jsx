import './TeachersDashboardPage.css';
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from './../../../providers/AuthContext';
import AllQuizzesTeachers from './../Create a quiz page/AllQuizzesTeachers';


const CreatedQuizzesSection = () => {
  const { user } = useContext(AuthContext);

  return (
    <div className="CreatedQuizzesSectionBoard">
      <Link to={'/categories'}> 
        <h3>Quizzes created by {user.full_name} </h3>
      </Link>
      <AllQuizzesTeachers />
    </div>
  );
};

export default CreatedQuizzesSection;
