import './TeachersDashboardPage.css';
import React from 'react';
import { Link } from 'react-router-dom';
import AllCategoriesTeachers from '../Categories page/AllCategoriesTeachers';


const CategoriesSection = () => {

  return (
    <div className="CategoriesSectionBoard">
      <Link to={'/category'}> 
        <h3>Categories</h3>
      </Link>
      <AllCategoriesTeachers />
    </div>
  );
};

export default CategoriesSection;