import './QuizzesTeachers.css';
import React, { useContext } from 'react';
import { useEffect, useState } from 'react';
import { getAllQuizzes } from '../../../requests/QuizzesRequests';
import SingleQuizTeachers from './SingleQuizTeachers';
import AuthContext from './../../../providers/AuthContext';

const AllQuizzesTeachers = () => {
  const { user } = useContext(AuthContext);
  const [quizzes, setQuizzes] = useState([]);
  const end = 5;

  useEffect(() => {
    getAllQuizzes()
      .then((data) => setQuizzes(data))
      .catch(e => console.log(e));

  }, []);

  console.log('quizzesTeachers',  quizzes);

  return (
    <div className='AllQuizzesTeacher'>
      <button className="btn-createQuiz">
        Create quiz
      </button>
      {quizzes
        .filter(q => q.author_id === user.id)
        .sort((a, b) => b.id - a.id)
        .slice(0, end)
        .map(quiz => 
          <SingleQuizTeachers key={quiz.id} quiz={quiz} />
        )
      }
    </div>
  );
};

export default AllQuizzesTeachers;
