import './QuizzesTeachers.css';
import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


const SingleQuizTeachers = ({ quiz }) => {
  return (
    <Card border="secondary" style={{ marginBottom: '10px', marginTop: '15px'}}>
      <Card.Body style={{ padding: '0.3rem 0.8rem' }}> 
        <Card.Text>
          <Row>
            <Col xs={3} style={{ color: 'var(--blue)', fontSize: '17px' }}>
              Title
            </Col>
            <Col xs={2} style={{ color: 'var(--blue)', fontSize: '17px' }}>
              Time limit
            </Col>
            <Col xs={3} style={{ color: 'var(--blue)', fontSize: '17px' }}>
              Category
            </Col>
            
            <Col xs={2} style={{ color: 'var(--blue)', fontSize: '17px' }}>
              Points
            </Col>
            <Col xs={2} style={{ color: 'var(--blue)', fontSize: '17px' }}>
              Visible
            </Col>
          </Row> 
          <Row>
            <Col xs={3} style={{ color: 'var(--blue)', fontSize: '20px' }}>
              <div className="sqt-link" >
                <Link to={`/quizzes/${quiz.quiz_title}/${quiz.id}`} >
                  {quiz.quiz_title}
                </Link>
              </div>
            </Col>
            <Col xs={2} style={{ color: 'var(--blue)', fontSize: '20px' }}>
              {quiz.time_limit_minutes} min
            </Col>
            <Col xs={3} style={{ color: 'var(--blue)', fontSize: '20px' }}>
              {quiz.category}
            </Col>
            <Col xs={2} style={{ color: 'var(--blue)', fontSize: '20px' }}>
              {quiz.full_points}
            </Col>
            <Col xs={2} style={{ color: 'var(--blue)', fontSize: '20px' }}>
              {quiz.is_visible === 1
                ? <span style={{ color: 'var(--green)', fontSize: '20px' }}>
                  yes
                </span> 
                : <span style={{ color: 'var(--red)', fontSize: '20px' }}>
                  no
                </span>  
              }
            </Col>
          </Row>       
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default SingleQuizTeachers;
