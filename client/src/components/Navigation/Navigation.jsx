import './Navigation.css';
import React from 'react';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import QuizLogo from './QuizLogo.png';
import AuthContext from './../../providers/AuthContext';
import { STUDENT_ROLE, TEACHER_ROLE } from './../../common/constants';
import { Button, Navbar, Container, Nav } from 'react-bootstrap';

const Navigation = () => {
  const { user, setUser } = useContext(AuthContext);

  const logoutUser = () => {
    localStorage.removeItem('token');
    setUser(null);
  };

  return (
    <div>
      <Navbar className="Navbar">
        <Container>
          <Navbar.Brand>
            <img src={QuizLogo} alt="quizlogo" className="logo" />
          </Navbar.Brand>
          { user  
            ?<Nav> 
              <Nav.Link >
                <Link to='/dashboard' className="nav-links">
                    Dashboard 
                </Link>
              </Nav.Link>
              {
                (user.role === STUDENT_ROLE) 
                  ? <>
                    <Nav.Link >
                      <Link to='/categories' className="nav-links">
                          Categories  
                      </Link>
                    </Nav.Link>
                    <Nav.Link >
                      <Link to='/quiz/solve' className="nav-links">
                          Solve quiz   
                      </Link>
                    </Nav.Link>
                    <Nav.Link >
                      <Link to='/leaderboard' className="nav-links">
                          Leaderboard   
                      </Link>
                    </Nav.Link>
                    <Nav.Link >
                      <Link to='/history' className="nav-links">
                          History   
                      </Link>
                    </Nav.Link>
                  </>
                  : <div></div>
              }
              {
                (user.role === TEACHER_ROLE) 
                  ? <>
                    <Nav.Link >
                      <Link to='/category' className="nav-links">
                          Categories    
                      </Link>
                    </Nav.Link>
                    <Nav.Link >
                      <Link to='/quizzes/create' className="nav-links">
                          Create a quiz    
                      </Link>
                    </Nav.Link>
                    <Nav.Link >
                      <Link to='/quizzes/view' className="nav-links">
                          View Quiz     
                      </Link>
                    </Nav.Link>
                  </>
                  : <div></div>
              }
              <Nav.Link >
                <Button variant="outline-light" onClick={logoutUser}>
                      Log Out
                </Button>
              </Nav.Link>
            </Nav>
            : <div></div>
          }
        </Container>
      </Navbar>
    </div>
  );
};

export default Navigation;
