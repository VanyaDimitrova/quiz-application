import React from 'react';

const NotFound = () => {
  return (
    <div className="App">
      <div className="NotFound">
        <h2>Resource not Found!</h2>
      </div>
    </div>
  );
};

export default NotFound;
