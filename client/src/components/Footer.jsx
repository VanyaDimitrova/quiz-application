import React from 'react';

const Footer = () => {
  const date = new Date();
  const currentYear = date.getFullYear();
  return (
    <div className="Footer"> 
      <small> 
        &copy; 2021-{currentYear} Vanya. All Rights Reserved. 
      </small> 
    </div>
  );
};

export default Footer;
