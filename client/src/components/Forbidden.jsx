import React from 'react';

const Forbidden = () => {
  return (
    <div className="App">
      <div className="Forbidden">
        <h3>You are not authorized for this resource!</h3>
      </div>
    </div>
  );
};

export default Forbidden;
