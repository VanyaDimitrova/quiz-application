import MAIN_URL from '../common/MainUrl';
import token from '../services/auth/get-token';

export const getAllCategories = () => {
  return fetch(`${MAIN_URL}/categories`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${token()}`,
    }
  })
    .then((res) => res.json());
};

export const createCategory = (data) => {
  return fetch(`${MAIN_URL}/categories`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token()}`,
    },
    body: JSON.stringify(data)
  })
    .then((response) => response.json());
};
