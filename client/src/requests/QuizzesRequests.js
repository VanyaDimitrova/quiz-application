import MAIN_URL from '../common/MainUrl';
import token from '../services/auth/get-token';

export const getAllQuizzes = () => {
  return fetch(`${MAIN_URL}/quizzes`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${token()}`,
    }
  })
    .then((res) => res.json());
};

export const getQuizzesByCategory = (id) => {
  return fetch(`${MAIN_URL}/quizzes/category/${id}`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${token()}`,
    }
  })
    .then((res) => res.json());
};

export const getAllSolvedQuizzes = () => {
  return fetch(`${MAIN_URL}/quizzes/solved`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${token()}`,
    }
  })
    .then((res) => res.json());
};

export const getQuizzesSolvedByUser = (user_id) => {
  return fetch(`${MAIN_URL}/quizzes/solved/${user_id}`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${token()}`,
    } 
  })
    .then((res) => res.json());
};

export const getQuizzesSolvedByUserByCategory = (user_id, category_id) => {
  return fetch(`${MAIN_URL}/quizzes/solved/${user_id}/category/${category_id}`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${token()}`,
    } 
  })
    .then((res) => res.json());
};
