import MAIN_URL from './../common/MainUrl';
import token from './../services/auth/get-token';


export const loginUser = (data) => {
  return fetch(`${MAIN_URL}/auth/signin`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token()}`,
    },
    body: JSON.stringify(data)
  });
};

export const createUser = (data) => {
  return fetch(`${MAIN_URL}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  });
};
