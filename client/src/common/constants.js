
export const STUDENT_ROLE = 'student';
export const TEACHER_ROLE = 'teacher';
export const DEFAULT_USER_ROLE = STUDENT_ROLE;

export const isTeacher = (user) => user?.role === TEACHER_ROLE;
export const isStudent = (user) => user?.role === STUDENT_ROLE;
