import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

// auth={!!user} toLog="/login" role={user.role} toPath="/forbidden"
const GuardedRoute = ({ component: Component, auth, toLog, roleCorrect, toPath, ...rest }) => (
  <Route {...rest} render={(props) => {
    return (
      auth 
        ? ( roleCorrect
          ? <Component {...props}/>
          : <Redirect to={toPath} />
        )
        : <Redirect to={toLog} />
    );
  }}
  />
);


GuardedRoute.propTypes = {
  component: PropTypes.func.isRequired,
  auth: PropTypes.bool.isRequired,
  toLog: PropTypes.string.isRequired,
  role: PropTypes.bool,
  toPath: PropTypes.string,
};

export default GuardedRoute;