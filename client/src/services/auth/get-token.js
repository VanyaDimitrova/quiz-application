
const token = () => {

  const token = localStorage.getItem('token');

  if (!token) {
    return '';
  }

  return token;
};

export default token;
