import './App.css';
import { useState } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import { isStudent, isTeacher } from './common/constants';
import getUserFromToken from './services/auth/user-from-token';
import AuthContext from './providers/AuthContext';
import GuardedRoute from './providers/GuardedRoute';

import NotFound from './components/NotFound';
import Forbidden from './components/Forbidden';
import Navigation from './components/Navigation/Navigation';
import LandingPage from './components/Landing page/LandingPage';
import Footer from './components/Footer';
import Home from './components/Home/Home';

import CategoriesPage from './components/Students/Categories/CategoriesPage';
import CategoryQuizzes from './components/Students/Categories/Quizzes/CategoryQuizzes';
import HistoryPage from './components/Students/History page/HistoryPage';
import AllStudentsLeaderboardPage from './components/Students/LeaderBoard page/AllStudentsLeaderboardPage';
import SolveQuizPage from './components/Students/Solve quiz page/SolveQuizPage';

import CategoryQuizzesTeacher from './components/Teachers/Categories page/CategoryQuizzesTeacher';
import CategoriesPageTeachers from './components/Teachers/Categories page/CategoriesPageTeachers';

function App() {
  const [ user, setUser ] = useState(getUserFromToken(localStorage.getItem('token')));
  console.log('user1', user);

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ user, setUser }}>
        <Navigation />
        <Switch>
          <Redirect path="/" exact to="/login" />
          <Route exact path="/login" component={LandingPage} />
          <GuardedRoute exact path="/dashboard" component={Home} auth={!!user} toLog="/login" roleCorrect={true} />

          <GuardedRoute exact path="/categories" component={CategoriesPage} auth={!!user} toLog="/login" roleCorrect={isStudent(user)} toPath="/forbidden" />
          <GuardedRoute exact path="/categories/:id" component={CategoryQuizzes} auth={!!user} toLog="/login" roleCorrect={isStudent(user)} toPath="/forbidden" />
          <GuardedRoute exact path="/history" component={HistoryPage} auth={!!user} toLog="/login" roleCorrect={isStudent(user)} toPath="/forbidden" />
          <GuardedRoute exact path="/leaderboard" component={AllStudentsLeaderboardPage} auth={!!user} toLog="/login" roleCorrect={isStudent(user)} toPath="/forbidden" />
          <GuardedRoute exact path="/quizzes/solve/:id" component={SolveQuizPage} auth={!!user} toLog="/login" roleCorrect={isStudent(user)} toPath="/forbidden" />

          <GuardedRoute exact path="/category" component={CategoriesPageTeachers} auth={!!user} toLog="/login" roleCorrect={isTeacher(user)} toPath="/forbidden" />
          <GuardedRoute exact path="/category/:id" component={CategoryQuizzesTeacher} auth={!!user} toLog="/login" roleCorrect={isTeacher(user)} toPath="/forbidden" />

          <Route path="/forbidden" component={Forbidden} />
          <Route path="*" component={NotFound} />
        </Switch>
        <Footer />
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
